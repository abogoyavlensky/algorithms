def sum_list(second):
    last = second.pop()
    second[-1] += last
    if len(second) > 1:
        return sum(second)
    else:
        return second.pop()


ex_list = range(10)

print sum_list(ex_list)
print sum(ex_list)


#properties and setters

class OurClass:

    def __init__(self, a):
        self.our_att = a

    @property
    def our_att(self):
        return self.__our_att

    @our_att.setter
    def our_att(self, val):
        self.__our_att = val


x = OurClass(10)
print(x.our_att)
x.our_att = 12
print x.our_att


#simple example of closure in Python
def foo():
    a = 10
    def foo2(a=a):
        print a + 1
        return
    return foo2

f = foo()
f()







