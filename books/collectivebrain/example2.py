# -*- coding: utf-8 -*-

__author__ = 'Andrey Bogoyavlensky'
__email__ = 'abogoyavlensky@gmail.com'
__copyright__ = 'Copyright 2015, Rambler&Co'

import plotly.plotly as plotly
import plotly.graph_objs as graph_objs


def func(user, api_key):
    plotly.sign_in(user, api_key)
    trace0 = graph_objs.Scatter(
        x=[1, 2, 3, 4],
        y=[10, 15, 13, 17]
    )
    trace1 = graph_objs.Scatter(
        x=[1, 2, 3, 4],
        y=[16, 5, 11, 9]
    )
    data = graph_objs.Data([trace0, trace1])

    unique_url = plotly.plot(data, filename='basic-line')
    return unique_url