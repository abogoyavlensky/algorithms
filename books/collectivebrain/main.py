# -*- coding: utf-8 -*-

__author__ = 'Andrey Bogoyavlensky'
__email__ = 'abogoyavlensky@gmail.com'
__copyright__ = 'Copyright 2015, Rambler&Co'

from books.collectivebrain.example2 import func
from books.collectivebrain.settings import PLOTLY_USER, PLOTLY_API_KEY

if __name__ == '__main__':
    url = func(PLOTLY_USER, PLOTLY_API_KEY)
    print url