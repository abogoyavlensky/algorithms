def qsort(seq):
    if len(seq) <= 1:
        return seq
    else:
        pivot = seq[0]
        l_seq, r_seq = [], []
        for i in seq[1:]:
            if i > pivot:
                r_seq.append(i)
            else:
                l_seq.append(i)
        return qsort(l_seq) + [pivot] + qsort(r_seq)