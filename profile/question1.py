def make_dict(keys, values):
    """Make new dict from two lists with keys and values.

    :param keys: list with keys
    :type keys: list
    :param values: list with values
    :type values: list
    :return: dict

    The example of usage:

    >>> make_dict(['a', 'b', 'c'], [12, 234, 5, 7])
    {'a': 12, 'c': 5, 'b': 234}
    >>> make_dict(['a', 'b', 'c'], [12, 234])
    {'a': 12, 'c': None, 'b': 234}
    """
    # return dict((key, values[i] if len(values) > i else None) for i, key in enumerate(keys))
    return dict(map(None, keys, values[:len(keys)]))


if __name__ == "__main__":
    keys, values = ['a', 'b', 'c', 'd'], [12, 234, 5, 7, 8, 9]
    print make_dict(keys, values)

    keys, values = ['a', 'b', 'c', 'd'], [12, 234]
    print make_dict(keys, values)


    keys = []
    values = [12, 234, 5, 7, 8, 9]
    print make_dict(keys, values)

    keys = ['a', 'b', 'c', 'd', 'f', 'g', 'h']
    values = []
    print make_dict(keys, values)

    keys = []
    values = []
    print make_dict(keys, values)