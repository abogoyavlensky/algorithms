import os


def top_ips(file_path):
    """Count the number of unique ip in a Nginx log file and print first ten ips.

    :param file_path: oath to log file
    :type file_path: str
    :return: list of tuples

    The example of usage:

    >>> top_ips("/var/log/nginx/access.log")
    [('64.186.146.87', 461), ('176.78.34.247', 14), ('198.20.69.74', 4),
     ('54.205.251.140', 2), ('148.251.124.238', 1), ('182.254.140.205', 1),
     ('89.248.161.57', 1)]
    """
    if os.path.isfile(file_path):
        ip_list = {}
        f = open(file_path, "r").readlines()
        for line in f:
            ip = line.split()[0]
            ip_list[ip] = ip_list.get(ip, 0) + 1

        return sorted(ip_list.items(), key=lambda x: x[1], reverse=True)[:10]
    else:
        raise Exception('File does not exist')


if __name__ == "__main__":
    print top_ips("profile/nginx.access.log")