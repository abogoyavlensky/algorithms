#!/bin/bash
awk '{ print $1 }' /var/log/nginx/access.log | sort -n | uniq -c | sort -rn | head
