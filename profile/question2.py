import re
import time


def check_time(f):
    def f_timer(*args, **kwargs):
        start = time.time()
        result = f(*args, **kwargs)
        end = time.time()
        print f.__name__, 'took', end - start, 'time'
        return result
    return f_timer


@check_time
def validate_login_re(login):
    """Validate login using regex.

    :param login: string variable with login
    :type login: str
    :return: bool

    The example of usage:

    >>> validate_login_re('Name')
    True
    >>> validate_login_re('Name.394.-.9frE0')
    True
    >>> validate_login_re('Name394.-.9frE0-')
    False
    >>> validate_login_re('Name394.-.9frE0aaaaaaaaaaaaaaaaa')
    False
    >>> validate_login_re('1Andrey')
    False
    >>> validate_login_re('')
    False
    >>> validate_login_re('First Name')
    False
    """
    regex = "(^[a-zA-Z]{1})([a-zA-Z0-9\.\-]{0,18}[a-zA-Z0-9]|$)$"
    return True if re.match(regex, login.strip()) else False


@check_time
def validate_login_cmp(login):
    """Validate login by checking entrance in list of latin characters
    and comparison.

    :param login: string variable with login
    :type login: str
    :return: bool

    The example of usage:

    >>> validate_login_cmp('Name')
    True
    >>> validate_login_cmp('Name.394.-.9frE0')
    True
    >>> validate_login_cmp('Name394.-.9frE0-')
    False
    >>> validate_login_cmp('Name394.-.9frE0aaaaaaaaaaaaaaaaa')
    False
    >>> validate_login_cmp('1Andrey')
    False
    >>> validate_login_cmp('')
    False
    >>> validate_login_cmp('First Name')
    False
    """

    if not login or len(login) > 20:
        return False

    login = login.lower()
    latin = map(chr, range(97, 123))
    latin_ext = latin + ['.', '-']

    if login[0] not in latin:
        return False

    if len(login) > 1 and (login[-1] not in latin and not login[-1].isdigit()):
        return False

    if len(login) > 2:
        for i in login[1:-1]:
            if i not in latin_ext and not i.isdigit():
                return False

    return True


if __name__ == "__main__":
    result_re = validate_login_re('Name394.-.9frE0aaaaa')
    #prints: validate_login_re took 0.00043511390686 time
    result_cmp = validate_login_cmp('Name394.-.9frE0aaaaa')
    #prints: validate_login_cmp took 1.69277191162e-05 time