#simple decorator
def my_dec(fn):
    def wrapped():
        print ('Hello ')
        result = fn()
        print '!'
        return result
    return wrapped


@my_dec
def foo():
    print 'World'

foo()


gen = (i for i in range(10))

for i in gen:
  print i
  
for i in gen:
  print i


#simple yield example
def GetGenerator(mylist):
    for i in mylist:
        yield i*i
    
mygen = GetGenerator([1,2,3,4,5])
print mygen

for i in mygen:
    print i
 
for i in mygen:
    print i